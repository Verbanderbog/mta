import {
  DiceRollerDialogue
} from "./dialogue-diceRoller.js";
import {
  ImprovisedSpellDialogue
} from "./dialogue-improvisedSpell.js";
/**
 * Override and extend the basic :class:`Actor` implementation
 */
export class ActorMtA extends Actor {

  /* -------------------------------------------- */
  /*	Data Preparation														*/
  /* -------------------------------------------- */

  /**
   * Augment the basic Actor data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();
    

    // Get the Actor's data object
    const actorData = this.data;
    const data = actorData.data;

    if(!data.derivedTraits) derivedTraits = {
      size: {value: 0, mod: 0},
      speed: {value: 0, mod: 0},
      defense: {value: 0, mod: 0},
      armor: {value: 0, mod: 0},
      ballistic: {value: 0, mod: 0},
      initiativeMod: {value: 0, mod: 0},
      perception: {value: 0, mod: 0},
      health: {value: 0, mod: 0}
    };

    //Get modifiers from items
    let item_mods = actorData.items.reduce((acc, item) => {
      if (item.data.data.equipped) {
        if (item.data.data.initiativeMod) acc.initiativeMod += item.data.data.initiativeMod;
        if (item.data.type === "armor") acc.armor += item.data.data.rating;
        if (item.data.type === "armor") acc.ballistic += item.data.data.ballistic;
        if (item.data.data.defensePenalty) acc.defense -= item.data.data.defensePenalty;
        if (item.data.data.speedPenalty) acc.speed -= item.data.data.speedPenalty;
      }
      return acc;
    }, {
      initiativeMod: 0,
      defense: 0,
      speed: 0,
      armor: 0,
      ballistic: 0
    });

    let attributes = [];
    if (actorData.type === "character") {
      attributes = [data.attributes_physical, data.attributes_mental, data.attributes_social, data.skills_physical, data.skills_social, data.skills_mental, data.derivedTraits, data.attributes_physical_dream, data.attributes_mental_dream, data.attributes_social_dream];
    }
    else if (actorData.type === "ephemeral"){
      attributes = [data.eph_physical, data.eph_mental, data.eph_social, data.derivedTraits];
    }
    
    attributes.forEach(attribute => Object.values(attribute).forEach(trait => {
      if(typeof trait == 'number') trait = {}; // Quick fix for a mistake I made for dream stats
      trait.final = trait.value;
      trait.raw = undefined;
      trait.isModified = false;
    }));

    const der = data.derivedTraits;

    let derivedTraitBuffs = [];
    let itemBuffs = [];

    //Get effects from items (modifiers to any attribute/skill/etc.)
    for (let i of actorData.items) {
      if (i?.data?.data?.effects && i?.data?.data?.effectsActive) { // only look at active effects
        if(i.type === "form" && actorData.data.characterType !== "Werewolf") continue; // Forms only work for werewolves
        itemBuffs = itemBuffs.concat(i.data.data.effects);
      }
    }

    itemBuffs.filter( e => e.name.split('.')[0] !== "derivedTraits" ).sort( (a,b) => (b.value<0)-(a.value<0) || (!!a.overFive)-(!!b.overFive) ).forEach( e => {
      const trait = e.name.split('.').reduce((o,i) => {
        if(o && o[i]) return o[i];
        else return undefined;
      }, data);
      if(trait) {
        const newVal = (Number.isInteger(trait.raw) ? trait.raw : trait.value ) + e.value;
        trait.raw = e.overFive  ?  newVal  :  Math.min( newVal, Math.max(trait.value, this.getTraitMaximum()) );
        trait.final = Math.clamped(trait.raw, 0, Math.max(trait.value,CONFIG.MTA.traitMaximum));
        trait.isModified = true;
      }
    });  
    derivedTraitBuffs.push(...itemBuffs.filter( e => e.name.split('.')[0] === "derivedTraits" ));

    // Compute derived traits
    if (actorData.type === "character") {
      const str = data.attributes_physical.strength.final;
      const dex = data.attributes_physical.dexterity.final;
      const wit = data.attributes_mental.wits.final;
      const comp = data.attributes_social.composure.final;

      if(data.isDreaming) {
        der.speed.final = 5 + data.attributes_physical_dream.power.final + data.attributes_social_dream.finesse.final;
        der.defense.final = Math.min(data.attributes_physical_dream.power.final, data.attributes_social_dream.finesse.final);
        der.initiativeMod.final = data.attributes_social_dream.finesse.final + data.attributes_mental_dream.resistance.final + der.initiativeMod.mod + item_mods.initiativeMod;

        let newMax = 0;
        if(data.characterType === "Changeling") newMax = data.clarity.value;
        else newMax = data.attributes_mental_dream.resistance.final;
        
        //  Add Gnosis/Wyrd derived maximum
        if(data.characterType === "Mage" || data.characterType === "Scelesti") newMax += Math.max(5, data.mage_traits.gnosis);
        else if (data.characterType === "Changeling") newMax += Math.max(5, data.changeling_traits.wyrd);
        else newMax += 5;
        der.health.value = newMax;
      }
      else {
        der.speed.value = 5 + str + dex;
        der.defense.value = Math.min(wit, dex) + this._getDefenseSkill();
        der.initiativeMod.value = comp + dex;
        der.health.value = data.attributes_physical.stamina.final;
      }
      der.perception.value = comp + wit + this.getClarityBonus();
    }
    else if (actorData.type === "ephemeral"){
      der.speed.value = 5 + data.eph_physical.power.final + data.eph_social.finesse.final;
      der.defense.value = (data.rank > 1 ? Math.min(data.eph_physical.power.final, data.eph_social.finesse.final) : Math.max(data.eph_physical.power.final, data.eph_social.finesse.final));
      der.initiativeMod.value = data.eph_social.finesse.final + data.eph_mental.resistance.final;
      der.perception.value = data.eph_social.finesse.final + data.eph_mental.resistance.final;
      der.health.value = data.eph_physical.power.final;
    }

    der.size.value = 5 + der.size.mod;
    der.armor.value = der.armor.mod + item_mods.armor;
    der.ballistic.value += der.ballistic.mod + item_mods.ballistic;

    der.speed.value += der.speed.mod + item_mods.speed;
    der.defense.value += der.defense.mod + item_mods.defense;
    der.initiativeMod.value += der.initiativeMod.mod + item_mods.initiativeMod;
    der.perception.value += der.perception.mod;
    der.health.value += der.health.mod;

    [data.derivedTraits].forEach(attribute => Object.values(attribute).forEach(trait => {
      trait.final = trait.value;
      trait.raw = undefined;
      trait.isModified = false;
    }));

    // Apply derived Traits buffs
    derivedTraitBuffs.forEach(e => {
      const trait = e.name.split('.').reduce((o,i)=> o[i], data);
      trait.raw = Number.isInteger(trait.raw) ?  trait.raw + e.value : trait.value + e.value;
      trait.final = Math.max(trait.raw, 0);
      trait.isModified = true;
    });

    if(!data.isDreaming) der.health.final += der.size.final;

    der.size.final = Math.max(0, der.size.final);
    der.speed.final = Math.max(0, der.speed.final);
    der.defense.final = Math.max(0, der.defense.final);
    der.armor.final = Math.max(0, der.armor.final);
    der.ballistic.final = Math.max(0, der.ballistic.final);
    der.initiativeMod.final = Math.max(0, der.initiativeMod.final);
    der.perception.final = Math.max(0, der.perception.final);

    //der.initiativeMod.computed = ActorMtA.getTrait(der.initiativeMod); // TODO: Remove (no longer necessary)
    
    // Get current demon cover
    if(data.characterType === "Demon"){
      data.currentCover = 0;
      for(let actorItem of actorData.items) {
        if(actorItem.data.type == "cover" && actorItem.data.data.isActive) {
          data.currentCover = actorItem.data.data.rating;
          data.currentCoverName = actorItem.data.name;
          break;
        }
      }
    }
  }

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers
  /* -------------------------------------------- */

  /** @override */
  static async create(data, options = {}) {
    data.token = data.token || {};
    if (data.type === "character") {
      mergeObject(data.token, {
        vision: false,
        dimSight: 30,
        brightSight: 0,
        actorLink: false,
        disposition: 0,
        displayBars: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER,
        bar1: {
          attribute: "health"
        },
        displayName: CONST.TOKEN_DISPLAY_MODES.OWNER_HOVER
      }, {
        overwrite: false
      });
    }

    const actor = await super.create(data, options);
    if(actor.data.type === "character") await actor.createWerewolfForms();
    
    return actor;
  }

  /**
   * Creates the 5 standard werewolf forms for the actor, and
   * deletes all existing forms.
   */
  async createWerewolfForms(){
    //Add the 5 basic werewolf forms
    const itemData = [
      {
        name: "Hishu",
        type: "form",
        img: "systems/mta/icons/forms/Hishu.svg",
        data: {
          subname: "Human",
          effects: [
            {name: "derivedTraits.perception", value: 1, overFive: true}
          ],
          description_short: "Sheep's Clothing",
          description: "",
          effectsActive: true
        }
      },
      {
        name: "Dalu",
        type: "form",
        img: "systems/mta/icons/forms/Dalu.svg",
        data: {
          subname: "Near-Human",
          effects: [
            {name: "attributes_physical.strength", value: 1, overFive: true},
            {name: "attributes_physical.stamina", value: 1, overFive: true},
            {name: "attributes_social.manipulation", value: -1, overFive: true},
            {name: "derivedTraits.size", value: 1, overFive: true},
            {name: "derivedTraits.perception", value: 2, overFive: true}
          ],
          description_short: "Teeth/Claws +0L\nDefense vs. Firearms\nMild Lunacy\nBadass Motherfucker",
          description: ""
        }
      },
      {
        name: "Gauru",
        type: "form",
        img: "systems/mta/icons/forms/Gauru.svg",
        data: {
          subname: "Wolf-Man",
          effects: [
            {name: "attributes_physical.strength", value: 3, overFive: true},
            {name: "attributes_physical.dexterity", value: 1, overFive: true},
            {name: "attributes_physical.stamina", value: 2, overFive: true},
            {name: "derivedTraits.size", value: 2, overFive: true},
            {name: "derivedTraits.perception", value: 3, overFive: true}
          ],
          description_short: "Teeth/Claws +2L\n(Initiative +3)\nDefense vs. Firearms\nFull Lunacy\nRegeneration\nRage\nPrimal Fear",
          description: ""
        }
      },
      {
        name: "Urshul",
        type: "form",
        img: "systems/mta/icons/forms/Urshul.svg",
        data: {
          subname: "Near-Wolf",
          effects: [
            {name: "attributes_physical.strength", value: 2, overFive: true},
            {name: "attributes_physical.dexterity", value: 2, overFive: true},
            {name: "attributes_physical.stamina", value: 2, overFive: true},
            {name: "attributes_social.manipulation", value: -1, overFive: true},
            {name: "derivedTraits.size", value: 1, overFive: true},
            {name: "derivedTraits.speed", value: 3, overFive: true},
            {name: "derivedTraits.perception", value: 3, overFive: true}
          ],
          description_short: "Teeth +2L/Claws +1L\nDefense vs. Firearms\nModerate Lunacy\nWeaken the Prey",
          description: ""
        }
      },
      {
        name: "Urhan",
        type: "form",
        img: "systems/mta/icons/forms/Urhan.svg",
        data: {
          subname: "Wolf",
          effects: [
            {name: "attributes_physical.dexterity", value: 2, overFive: true},
            {name: "attributes_physical.stamina", value: 1, overFive: true},
            {name: "attributes_social.manipulation", value: -1, overFive: true},
            {name: "derivedTraits.size", value: -1, overFive: true},
            {name: "derivedTraits.speed", value: 3, overFive: true},
            {name: "derivedTraits.perception", value: 4, overFive: true}
          ],
          description_short: "Teeth +1L\nChase Down",
          description: ""
        }
      }
    ];
    let oldForms = this.data.items.filter(item => item.type === "form").map(item => item.id);
    if(oldForms) await this.deleteEmbeddedDocuments("Item",oldForms);
    await this.createEmbeddedDocuments("Item", itemData);
  }

  /**
   * TODO: DEPRECATED
   * This function returns the actual, modified value for any attribute or skill.
   * and should be used whenever someone rolls.
   * Final is the modified trait, and value is the base.
   * Do not use this for derived traits! They do not have value or final fields.
   */
  static getTrait(trait){
    console.warn("CofD system: The getTrait() function has been deprecated. Please simply use trait.final instead.")
    return trait.final;
    if(!trait) {
      ui.notifications.error("The requested trait does not exist (" + trait + ")");
      return 0;
    }
    return Number.isInteger(trait.final) ? trait.final : trait.value;
  }

  //Search for Merit Defensive Combat
  _getDefenseSkill() {
    const actorData = this.data;
    const data = actorData.data;

    const hasBrawlMerit = this.data.items.find(ele => {
      return ele.name === "Defensive Combat (Brawl)" && ele.type === "merit";
    });
    let hasWeaponryMerit = this.data.items.find(ele => {
      return ele.name === "Defensive Combat (Weaponry)" && ele.type === "merit";
    });
    if (hasWeaponryMerit) {
      hasWeaponryMerit = this.data.items.find(ele => {
        return ele.data.data.equipped && ele.type === "melee";
      });
    }

    const brawlSkill = hasBrawlMerit ? data.skills_physical.brawl.final  : 0;
    const weaponrySkill = hasWeaponryMerit ? data.skills_physical.weaponry.final : 0;
    return Math.max(Math.max(brawlSkill, weaponrySkill), data.skills_physical.athletics.final);
  }

  /** Returns the attribute limit of the character (e.g. Gnosis for mages) **/
  getTraitMaximum() {
    const data = this.data.data;
    if(this.data.type === "ephemeral")
      return 999;
  
    const powerStats = { //TODO: Put in config
      Mortal: 5,
      Sleepwalker: 5,
      Mage: data.mage_traits.gnosis,
      Scelesti: data.mage_traits.gnosis,
      Proximi: 5,
      Vampire: data.vampire_traits.bloodPotency,
      Changeling: data.changeling_traits.wyrd,
      Werewolf: data.werewolf_traits.primalUrge,
      Demon: data.demon_traits.primum
    };
    return Math.max(5,powerStats[data.characterType]);
  }

  /**
   * Executes a perception roll using Composure + Wits.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollPerception(quickRoll, hidden, actorOverride) {
    const data = this.data.data;
    let dicepool = data.derivedTraits.perception.final;
    let flavor = "Perception";
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      title: flavor,
      blindGMRoll: hidden,
      actorOverride: actorOverride
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        title: flavor,
        addBonusFlavor: true,
        blindGMRoll: true,
        actorOverride: actorOverride
      });
      diceRoller.render(true);
    }
  }

  getWoundPenalties() {
    const data = this.data.data;
    let woundPenalty = 0;
    if(data.health.value <= 3 && !(this.type === "ephemeral")) {
      woundPenalty = 2 - (data.health.value-1);
      // Check for Iron Stamina Merit
      let ironStamina = this.items.find(item => item.type === "merit" && item.name === "Iron Stamina");
      if(ironStamina) woundPenalty = Math.max(0, woundPenalty - ironStamina.data.data.rating);
    }
    return woundPenalty;
  }

  // 
  /**
   * Gets the Perception bonus dependent on Clarity (only Changelings).
   * In contrast to wound getWoundPenalties, penalties here are negative.
   */
  getClarityBonus() {
    const data = this.data.data;
    if(data.characterType !== "Changeling") return 0;
    let clarity = data.clarity.value;
    let clarityMax = data.clarity.max;

    let diceBonus = (clarity < 3) ? -2 : (clarity < 5) ? -1 : 0;
    if(clarity === clarityMax) diceBonus += 2;
    if(clarity <= 0) diceBonus = -99;

    return diceBonus;
  }

  castSpell(spell){
    const itemData = spell ? duplicate(spell.data.data) : {};
    if (spell) {
      if (spell.data.data.isRote) itemData.castRote = true;
      else if (spell.data.data.isPraxis) itemData.castPraxis = true;
    }

    let activeSpell = new CONFIG.Item.documentClass({
      data: mergeObject(game.system.model.Item["activeSpell"], itemData, {
        inplace: false
      }),
      name: spell ? spell.name : 'Improvised Spell',
      img: spell ? spell.img : '',
      type:  "activeSpell"
    }); 

    activeSpell.data.img = spell ? spell.img : '';
    
    let spellDialogue = new ImprovisedSpellDialogue(activeSpell, this);
    spellDialogue.render(true);
  }

  /**
   * Executes a breaking point roll using Resolve + Composure.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollBreakingPoint(quickRoll, hidden) {
    const data = this.data.data;
    let dicepool = data.attributes_social.composure.final + data.attributes_mental.resolve.final;
    let penalty = data.integrity >= 8 ? 2 : data.integrity >= 6 ? 1 : data.integrity <= 1 ? -2 : data.integrity <= 3 ? -1 : 0;
    dicepool += penalty;
    let flavor = "Breaking Point: Resolve + Composure + " + penalty;
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      title: flavor,
      blindGMRoll: hidden
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        title: flavor,
        addBonusFlavor: true,
        blindGMRoll: hidden
      });
      diceRoller.render(true);
    }
  }

  /**
   * Executes a dissonance roll using Integrity.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
  rollDissonance(quickRoll, hidden) {
    const data = this.data.data;
    let dicepool = data.integrity;
    let flavor = "Dissonance: Integrity (withstood by spell dots, rank, soul stones, etc.)";
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      title: flavor,
      blindGMRoll: hidden
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        title: flavor,
        addBonusFlavor: true,
        blindGMRoll: hidden
      });
      diceRoller.render(true);
    }
  }

  /**
   * Executes a cover compromise roll using Wits + Manipulation.
   * if hidden is set, the roll is executed as a blind GM roll.
   */
   rollCompromise(quickRoll, hidden) {
    const data = this.data.data;
    let dicepool = data.attributes_mental.wits.final + ata.attributes_social.manipulation.final;
    let penalty = data.currentCover >= 8 ? 2 : data.currentCover >= 6 ? 1 : data.currentCover <= 1 ? -2 : data.currentCover <= 3 ? -1 : 0;
    dicepool += penalty;
    let flavor = "Compromise: Wits + Manipulation + " + penalty;
    if (quickRoll) DiceRollerDialogue.rollToChat({
      dicePool: dicepool,
      flavor: flavor,
      title: flavor,
      blindGMRoll: hidden
    });
    else {
      let diceRoller = new DiceRollerDialogue({
        dicePool: dicepool,
        flavor: flavor,
        title: flavor,
        addBonusFlavor: true,
        blindGMRoll: hidden
      });
      diceRoller.render(true);
    }
  }

  /**
   * Converts the character's stats into dream stats, 
   * depending on the template.
   */
  dreaming(unequipItems) {
    const data = this.data.data;
    const updateData = {};
    updateData['data.isDreaming'] = !data.isDreaming;
    if(updateData['data.isDreaming']) {
      // Start dreaming. Replace attributes and health.
      if(data.characterType === "Mage" || data.characterType === "Scelesti") updateData['data.attributes_physical_dream.power.value'] = Math.max( data.attributes_mental.intelligence.final, data.attributes_social.presence.final);
      else if (data.characterType === "Changeling") updateData['data.attributes_physical_dream.power.value'] = data.attributes_social.presence.final;
      else updateData['data.attributes_physical_dream.power.value'] = data.attributes_mental.intelligence.final;

      if(data.characterType === "Mage" || data.characterType === "Scelesti") updateData['data.attributes_social_dream.finesse.value'] = Math.max( data.attributes_mental.wits.final, data.attributes_social.manipulation.final);
      else if (data.characterType === "Changeling") updateData['data.attributes_social_dream.finesse.value'] = data.attributes_social.manipulation.final;
      else updateData['data.attributes_social_dream.finesse.value'] = data.attributes_mental.wits.final;
      
      if(data.characterType === "Mage" || data.characterType === "Scelesti") updateData['data.attributes_mental_dream.resistance.value'] = Math.max( data.attributes_mental.resolve.final, data.attributes_social.composure.final);
      else if (data.characterType === "Changeling") updateData['data.attributes_mental_dream.resistance.value'] = data.attributes_social.composure.final;
      else updateData['data.attributes_mental_dream.resistance.value'] = data.attributes_mental.resolve.final;

      // Slightly unusual: to make sure that token's health bars stll show the currently important health,
      // the normal health is backed up into dream_health, and health is replaced, instead of introducing
      // a new type of health as a new trait. Dream health is not backed up, as I believe that's not a thing.
      updateData['data.dream_health'] = data.health;
      let newMax = 0;
      if(data.characterType === "Changeling") newMax = data.clarity.value;
      else newMax = updateData['data.attributes_mental_dream.resistance.value'];
      
      //  Add Gnosis/Wyrd derived maximum
      if(data.characterType === "Mage" || data.characterType === "Scelesti") newMax += Math.max(5, data.mage_traits.gnosis);
      else if (data.characterType === "Changeling") newMax += Math.max(5, data.changeling_traits.wyrd);
      else newMax += 5;

      updateData['data.health'] = {
        max: newMax,
        lethal: newMax,
        aggravated: newMax,
        value: newMax
      }
      
    }
    else {
      // Dreaming ended. Reset health.
      if(data.dream_health) updateData['data.health'] = data.dream_health;
      let amnion = this.data.items.filter(item => item.name === "Amnion");
      if(amnion) this.deleteEmbeddedDocuments("Item", amnion.map(item => item.id));
    }
    if(unequipItems) {
      let equipped = this.data.items.filter(item => item.data.data.equipped);
      if(equipped) {
        this.updateEmbeddedDocuments("Item", equipped.map(item => {return {
        _id: item.id, 
        'data.equipped': false
      }}));
      }
    }
    this.update(updateData);
  }

  /**
   * A mage macro which conjures the amnion (as an item)
   * and equips it.
   */
  callAmnion() {
    const itemData = {
      type: "condition",
      name: "Amnion",
      img: "systems/mta/icons/gui/macro-amnion.svg",
      'data.effectsActive': true,
      'data.effects': [ 
        {
          name: "derivedTraits.armor",
          value: Math.min( this.data.data.mage_traits.gnosis, Math.max(...Object.values(this.data.data.arcana_subtle).map(arcanum => arcanum.value)))
        }, 
        {
          name: "attributes_social_dream.finesse",
          value: -2
        }, 
        {
          name: "derivedTraits.defense",
          value: -1
        }
      ]
    }
    return this.createEmbeddedDocuments("Item", [itemData]);
  }

  /**
   * Prompts the user with a dialogue to enter name and beats to add
   * a progress entry to the actor.
   */
  addProgressDialogue() {
    let d = new Dialog({
      title: "Add Progress",
      content: "<div> <span> Name </span> <input class='attribute-value' type='text' name='input.name' placeholder='No Reason'/></div> <div> <span> Beats </span> <input class='attribute-value' type='number' name='input.beats' data-dtype='Number' min='0' placeholder='0'/></div> <div> <span> Arc. Beats </span> <input class='attribute-value' type='number' name='input.arcaneBeats' data-dtype='Number' min='0' placeholder='0'/></div>",
      buttons: {
        ok: {
          icon: '<i class="fas fa-check"></i>',
          label: "OK",
          callback: html => {
            let name = html.find(".attribute-value[name='input.name']").val();
            if (!name) name = "No Reason";
            let beats = html.find(".attribute-value[name='input.beats']").val();
            if (!beats) beats = 0;
            let arcaneBeats = html.find(".attribute-value[name='input.arcaneBeats']").val();
            if (!arcaneBeats) arcaneBeats = 0;
            this.addProgress(name, beats, arcaneBeats);
          }
        },
        cancel: {
          icon: '<i class="fas fa-times"></i>',
          label: "Cancel"
        }
      },
      default: "cancel"
    });
    d.render(true);
  }

  /**
   * Adds a progress entry to the actor, with given name and beats.
   */
  addProgress(name = "", beats = 0, arcaneBeats = 0) {
    const data = this.data.data;
    beats = Math.floor(beats);
    arcaneBeats = Math.floor(arcaneBeats);
    let progress = data.progress ? duplicate(data.progress) : [];
    progress.push({
      name: name,
      beats: beats,
      arcaneBeats: arcaneBeats
    });
    return this.update({
      'data.progress': progress
    });
  }

  /**
   * Removes a progress entry from the actor at a given position.
   * Note, that the first entry (__INITIAL__) is not part of the progress array;
   * the element coming after it has index 0.
   */
  removeProgress(index = 0) {
    const data = this.data.data;
    let progress = data.progress ? duplicate(data.progress) : [];
    progress.splice(index, 1);
    return this.update({
      'data.progress': progress
    });
  }

  /**
   * Calculates and sets the maximum health for the actor using the formula
   * Stamina + Size.
   * If health is set lower than any damage, the damage is lost.
   */
  calculateAndSetMaxHealth() {
    const data = this.data.data;
    const maxHealth_old = data.health.max;
    //let maxHealth = data.derivedTraits.size.final + data.attributes_physical.stamina.final;
    let maxHealth = data.derivedTraits.health.final;
    //if(data.characterType === "Vampire") maxHealth += data.disciplines.common.resilience.value;

    let diff = maxHealth - maxHealth_old;
    if(diff === 0) return;

    let obj = {}
    obj['data.health.max'] = maxHealth;
    
    if (diff >= 0) { // New health is more than old
      obj['data.health.lethal'] = (+data.health.lethal + diff);
      obj['data.health.aggravated'] = (+data.health.aggravated + diff);
      obj['data.health.value'] = (+data.health.value + diff);
    } else { // New health is less than old
      obj['data.health.lethal'] = Math.max(0, (+data.health.lethal + diff));
      obj['data.health.aggravated'] = Math.max(0, (+data.health.aggravated + diff));
      obj['data.health.value'] = Math.max(0, (+data.health.value + diff));

      if(data.health.lethal < Math.abs(diff)) { // Too much lethal damage, upgrade lethal to aggravated damage.
        obj['data.health.aggravated'] = Math.max(0, obj['data.health.aggravated'] - Math.abs(Math.abs(diff) - data.health.lethal));
      }

      let diffBashing = Math.max(0, Math.abs(diff) - data.health.value);
      if(data.health.lethal < Math.abs(diff)) diffBashing -= Math.abs(Math.abs(diff) - data.health.lethal);
      if(diffBashing > 0) { // Too much bashing damage, upgrade bashing to lethal, or lethal to aggravated damage.
        obj['data.health.lethal'] -= diffBashing;
        if(obj['data.health.lethal'] < 0) {
          obj['data.health.aggravated'] = Math.max(0, obj['data.health.aggravated'] + obj['data.health.lethal']);
          obj['data.health.lethal'] = 0;
        }
      }
    }
    this.update(obj);
  }

  /**
   * Calculates and sets the maximum splat-specific ressource for the actor.
   * Mage: Mana (determined by Gnosis)
   * Vampire: Vitae (determined by Blood Potency)
   */
  calculateAndSetMaxResource() {
    const data = this.data.data;
    if (data.characterType === "Mage" || data.characterType === "Proximi" || data.characterType === "Scelesti") { // Mana
      let maxResource = (data.characterType === "Mage" || data.characterType === "Scelesti") ? CONFIG.MTA.gnosis_levels[Math.min(9, Math.max(0, data.mage_traits.gnosis - 1))].max_mana : 5;
      let obj = {}
      obj['data.mana.max'] = maxResource;
      this.update(obj);
    } else if (data.characterType === "Vampire") { // Vitae
      let maxResource = CONFIG.MTA.bloodPotency_levels[Math.min(10, Math.max(0, data.vampire_traits.bloodPotency))].max_vitae;
      if (data.vampire_traits.bloodPotency < 1) maxResource = data.attributes_physical.stamina.final

      let obj = {}
      obj['data.vitae.max'] = maxResource;
      this.update(obj);
    } else if (data.characterType === "Werewolf") { // Vitae
      let maxResource = CONFIG.MTA.primalUrge_levels[Math.min(9, Math.max(0, data.werewolf_traits.primalUrge - 1))].max_essence;

      let obj = {}
      obj['data.essence.max'] = maxResource;
      this.update(obj);
    } else if (data.characterType === "Demon") { // Vitae
      let maxResource = CONFIG.MTA.primum_levels[Math.min(9, Math.max(0, data.demon_traits.primum - 1))].max_aether;

      let obj = {}
      obj['data.aether.max'] = maxResource;
      this.update(obj);
    } 
  }

  /**
   * Calculates and sets the maximum clarity for the actor using the formula
   * Wits + Composure.
   * If clarity is set lower than any damage, the damage is lost.
   * Also calls updateChangelingTouchstones().
   */
  async calculateAndSetMaxClarity() {
    const data = this.data.data;
    const maxClarity_old = data.clarity.max;
    let maxClarity = data.attributes_mental.wits.final + data.attributes_social.composure.final;

    let obj = {}
    obj['data.clarity.max'] = maxClarity;

    let diff = maxClarity - maxClarity_old;
    if (diff > 0) {
      obj['data.clarity.severe'] = "" + (+data.clarity.severe + diff);
      obj['data.clarity.value'] = "" + (+data.clarity.value + diff);
    } else {
      obj['data.clarity.severe'] = "" + Math.max(0, (+data.clarity.severe + diff));
      obj['data.clarity.value'] = "" + Math.max(0, (+data.clarity.value + diff));
    }
    await this.update(obj);
    this.updateChangelingTouchstones();
  }

  /**
   * Updates the number of touchstones based on the maximum clarity.
   */
  updateChangelingTouchstones() {
    const data = this.data.data;
    let touchstones = duplicate(data.touchstones_changeling);
    let touchstone_amount = Object.keys(touchstones).length;
    if (touchstone_amount < data.clarity.max) {
      while (touchstone_amount < data.clarity.max) {
        touchstones[touchstone_amount + 1] = "";
        touchstone_amount = Object.keys(touchstones).length;
      }
    } else if (touchstone_amount > data.clarity.max) {
      while (touchstone_amount > data.clarity.max) {
        touchstones['-=' + touchstone_amount] = null;
        touchstone_amount -= 1;
      }
    }
    let obj = {};
    obj['data.touchstones_changeling'] = touchstones;
    this.update(obj);
  }
}